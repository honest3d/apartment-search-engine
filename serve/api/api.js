import express from 'express';
import containerRouter from './container/container.js';

const apiRouter = express.Router();

apiRouter.use('/container', containerRouter);

export default apiRouter;