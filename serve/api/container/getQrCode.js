import QRCode from 'qrcode';
import {PassThrough} from 'stream';
async function getQrCode(req,res){
    try{          
        const qrStream = new PassThrough();
        await QRCode.toFileStream(qrStream, `${process.env.BASE_URL}${req.params.qr}`,
            {
                type: 'png',
                width: req.params.width || 200,
                errorCorrectionLevel: 'H'
            }
        );

        qrStream.pipe(res);
    } catch(err){
        console.error('Failed to return content', err);
    }
}
export default getQrCode;