import DatabaseConnection from '../../db/connect.js';
import fs from 'fs';

function uploadPhotoToContainer(req,res) {
    if(!req.file){
        res.status(500)
            .send('Error: No file uploaded');
        return;
    }
    DatabaseConnection.Container.findOne({qr:req.params.qr})
        .then(container => {
            if(container.image){
                fs.unlink(`uploads/images/containers/${container.image}`, (err)=>{
                    if(err) console.log(err);
                });
            }
            container.image = req.file.filename;
            container.save();
            res.json(container);
        });

}
export default uploadPhotoToContainer;
