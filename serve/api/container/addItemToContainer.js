import DatabaseConnection from '../../db/connect.js';

function addItemToContainer(req,res){
    DatabaseConnection.addItemToContainer({qr:req.params.qr},req.params.name);
    res.json({added: req.params.name});
}

export default addItemToContainer;