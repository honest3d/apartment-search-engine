import DatabaseConnection from '../../db/connect.js';

function findOrCreateContainer(req,res){
    DatabaseConnection.Container.findOne({qr:req.params.qr}, (err, container)=>{
        if(err) throw err;
        if(container) {
            res.json(container);
        } else {
            DatabaseConnection.addNewContainer(req.params.qr);
            res.json({isNew:true});
        }
    });
}

export default findOrCreateContainer;

