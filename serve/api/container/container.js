import express from 'express';
import addItemToContainer from './addItemToContainer.js';
import findOrCreateContainer from './findOrCreateContainer.js';
import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import path from 'path';
import uploadPhotoToContainer from './uploadPhotoToContainer.js';
import getPhotoFromContainer from './getPhoto.js';
import getQrCode from './getQrCode.js';

const containerRouter = express.Router();

const qrRouter = express.Router();

const storage = multer.diskStorage({
    destination(req,file,callback) {
        callback(null,'uploads/images/containers');
    },
    filename:(req,file,cb)=>{
        cb(null, uuidv4() + path.extname(file.originalname));
    }
});
const upload = multer({storage,fileFilter:(req,file,cb)=>{
    if (file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg') {
        cb(null, true);
    } else {
        cb(null, false);
        return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    } 
}});



containerRouter.use('/qr', qrRouter);

qrRouter.get('/:qr', findOrCreateContainer);

qrRouter.get('/:qr/add/:name', addItemToContainer);

qrRouter.post('/:qr/upload', upload.single('image'), uploadPhotoToContainer);

qrRouter.get('/:qr/image/:width?/:height?/:fit?', getPhotoFromContainer);

qrRouter.get('/:qr/getqr/:width?', getQrCode);


export default containerRouter;