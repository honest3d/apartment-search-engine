import DatabaseConnection from '../../db/connect.js';
import resizeImage from '../../helpers/resizeImage.js';
function getPhotoFromContainer(req,res){
    DatabaseConnection.Container.findOne({qr:req.params.qr})
        .then(container=>{
            if(!container) {
                res.status(500)
                    .send('no container found');
                return;
            }
            if(!container.image){
                res.sendFile('/Gradient_builder_2.jpg.webp');
                return;
            }
            res.type('image/webp');
            resizeImage(`uploads/images/containers/${container.image}`,{width:req.params.width,height:req.params.height,fit:req.params.fit}).pipe(res);
        });
    
}
export default getPhotoFromContainer;