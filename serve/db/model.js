import mongoose from 'mongoose';

import dotenv from 'dotenv';
dotenv.config();

const Schema = mongoose.Schema;

const ItemSchema = new Schema({
    name: String,
    icon: String,
    color: String
});

const FlagSchema = new Schema({
    name: String,
    icon: String,
    color: String
});

const ContainerSchema = new Schema({
    name: {type: String, default:''},
    notes: {type: String, default:''},
    size: {type: Number, default:2},
    location: {type: String, default:''},
    qr: {type: String, unique: true},
    items: [ItemSchema],
    flags: [FlagSchema],
    image: String

});

export {ItemSchema, ContainerSchema};