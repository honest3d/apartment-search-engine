import mongoose from 'mongoose';

import { ItemSchema, ContainerSchema } from './model.js';

class DatabaseConnection {
    constructor() {
        this.database = mongoose.createConnection(process.env.MONGO);
        this.Container = this.database.model('Container', ContainerSchema);
        this.Item = this.database.model('Item', ItemSchema);
    }
    addNewContainer(qr,name,notes,size,location){
        const container = new this.Container({
            name,notes,size,location,qr
        });
        container.save();
    }
    addItemToContainer(containerQuery,name,icon){
        this.Container.findOne(containerQuery)
            .then(container=>{
                let item = new this.Item({name,icon});
                container.items.push(item);
                item.save();
                container.save();
            });
    }
}

export default new DatabaseConnection();