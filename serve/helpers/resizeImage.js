import sharp from 'sharp';
import fs from 'fs';

function resizeImage(path,{width,height,fit='cover'}){
    if(width && typeof width != 'number'){
        width = parseInt(width);
    }
    if(height && typeof height != 'number'){
        height = parseInt(height);
    }
    let imageStream = fs.createReadStream(path);
    let transform = sharp();
    if(width||height) transform = transform.resize({width,height,fit});
    transform = transform.toFormat('webp');
    return imageStream.pipe(transform);
}

export default resizeImage;