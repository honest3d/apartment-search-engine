import dotenv from 'dotenv';
dotenv.config();

import DatabaseConnection from './db/connect.js';
DatabaseConnection.database.on('open', ()=>console.log('database ready'));

import express from 'express';

import bodyParser from 'body-parser';

import apiRouter from './api/api.js';
const app = express();
app.use(bodyParser.urlencoded({
    extended:false
}));
app.use(bodyParser.json());

app.use('/api', apiRouter);

app.listen(process.env.PORT, ()=>{
    console.log(`api server started at port ${process.env.PORT}`);
});
